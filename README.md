# CONTENTS OF THIS FILE

  - Introduction
  - Requirements
  - Recommended modules
  - Installation
  - Maintainers


## INTRODUCTION

Slimbox 2 is a Drupal module that provides a wrapper to integrate
the easy to use Slimbox 2 "lightbox" plugin into Drupal websites.

Once installed, you can make any link open content in a "lightbox"
by setting the rel attribute of your link to rel="lightbox".
Your link should look something like:
<a href="path-to-some-image.png" rel="lightbox">link text or image</a>

For Lightbox Series use rel="lightbox-series":
<a href="path-to-some-image.png" rel="lightbox-series">link text or image</a>

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/slimbox2

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/slimbox2


## REQUIREMENTS

The Slimbox2 Drupal module requires the cbeyls/slimbox library 
(https://github.com/cbeyls/slimbox.git). Instructions on how to install 
the Slimbox library with Composer are detailed within the Installation Secion below. 

## RECOMMENDED MODULES

 * Image Link Attributes (https://www.drupal.org/project/image_link_attributes):
   Provides the ability to add classes and rel atttibutes to content image fields 
   that make it simple to create gallery page content types.

## INSTALLATION

 * Install Slimbox2 using Composer: composer require 'drupal/slimbox2'

 * Add the Slimbox library to composer.yml by adding the following markup to your 
   drupal website's composer.json file under the "repositories":[] section:
   {
      "type": "package",
      "package": {
            "name": "cbeyls/slimbox2",
            "version": "2.05",
            "type": "drupal-library",
            "source": {
               "url": "https://github.com/cbeyls/slimbox.git",
               "type": "git",
               "reference": "master"
            }
      }
   }

 * Install the library using composer: composer require 'cbeyls/slimbox2'
 
 * For more information about installing modules with Composer.
   See: https://www.drupal.org/docs/extending-drupal/installing-modules.

## MAINTAINERS

Current maintainers:
 * Andrew Wasson (awasson) - https://www.drupal.org/u/awasson
